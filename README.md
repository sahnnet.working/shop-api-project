# Shop API Project

## Table of Content

1. [Install PostgreSQL](#install-postgresql)
2. [Create a PostgreSQL user and database](#create-a-postgresql-user-and-database)
3. [Install Libraries](#install-libraries)
4. [Run Shop API](#run-shop-api)
5. [Run Web Crawlers](#run-web-crawlers)

___

### Install PostgreSQL

```
https://www.postgresqltutorial.com/postgresql-getting-started/install-postgresql/
```

### Create a PostgreSQL user and database

```
sudo -u postgres psql
```

```
CREATE DATABASE shopdb;
```

```
CREATE USER postgres WITH ENCRYPTED PASSWORD '123';
```

```
ALTER ROLE postgres SET client_encoding TO 'utf8';
ALTER ROLE postgres SET default_transaction_isolation TO 'read committed';
ALTER ROLE postgres SET timezone TO 'UTC';
```

```
GRANT ALL PRIVILEGES ON DATABASE shopdb TO postgres;
```

### Install Libraries

```
pip install -r requirements.txt 
```

### Run Shop API

```
cd shop_api
python manage.py runserver 127.0.0.1:8000
```

### Run Web Crawlers

```
cd web_crawlers
python main.py
```
