import asyncio
import platform
import crawlers.tezolmarket_crawler.crawl as tezolmarket
import crawlers.digikala_crawler.crawl as digikala

from functools import wraps
from typing import NoReturn
from asyncio.proactor_events import _ProactorBasePipeTransport


def silence_event_loop_closed(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except RuntimeError as e:
            if str(e) != 'Event loop is closed':
                raise

    return wrapper


async def main() -> NoReturn:
    tezolmarket_task = asyncio.create_task(tezolmarket.Crawler().run())
    digikala_task = asyncio.create_task(digikala.Crawler().run())

    await asyncio.wait([tezolmarket_task, digikala_task])


# Only when this is the original executable file is the condition true
if __name__ == '__main__':
    if platform.system() == 'Windows':
        _ProactorBasePipeTransport.__del__ = silence_event_loop_closed(_ProactorBasePipeTransport.__del__)

    asyncio.run(main())
