import asyncio
import aiohttp

from crawlers.abstract_crawler.crawl import CrawlerAbstract
from typing import NoReturn, Dict, List
from .storage import ShopAPIStorage
from utils.check import check_status


class Crawler(CrawlerAbstract):

    def __init__(self):
        self.base_url = 'https://api.digikala.com'

    async def make_all_url(self, category: str, min_page=1) -> List[str]:
        first_url_page = f"/v1/categories/{category}/search/?page=1"
        page_information = await self.downloader(url=first_url_page)

        # Searching is possible in up to 100 pages.
        min_page = min(min_page, 100)
        max_page = min(page_information['data']['pager']['total_pages'], 100)

        list_urls = [f"/v1/categories/{category}/search/?page={i}" for i in
                     range(min_page, max_page + 1)]

        return list_urls

    async def downloader(self, url: str) -> Dict:
        async with aiohttp.ClientSession(base_url=self.base_url) as session:
            async with session.get(url=url) as response:
                if await check_status(response.status, response.reason, url=f"{self.base_url}{url}"):
                    result = await response.json()  # JSON Response Content

        return result

    async def parser(self, product: dict, category_name: str) -> Dict:
        active = len(product['default_variant']) != 0
        if active:
            price = int(product['default_variant']['price']['selling_price'])
        else:
            price = 0
        data = {
            "title": product['title_fa'].replace('/', '.'),
            "active": active,
            "image_url": product['images']['main']['url'][0].split('?')[0],
            "category":
                {
                    "title": category_name,
                },
            "store":
                {
                    "name": "دیجیکالا",
                    "url": "https://www.digikala.com",
                    "image_url": f"https://www.digikala.com/statics/img/svg/logo.svg",
                },

            "store_product": {
                "url": f"https://www.digikala.com{product['url']['uri']}",
                "price": price,
            }
        }

        return data

    async def run(self) -> NoReturn:
        categories = ['breakfast', 'ready-made-canned-food']

        list_urls = await asyncio.gather(*[self.make_all_url(category=category) for category in categories])

        # Download information
        results = []
        for list_url in list_urls:
            results += await asyncio.gather(*[self.downloader(url=url) for url in list_url])

        # Pars information
        data = []
        for result in results:
            data += await asyncio.gather(
                *[self.parser(product=product, category_name=result['data']['intrack']['eventData']['name'])
                  for product in result['data']['products']])

        # Send data for shop api django project
        shop_api_storage = ShopAPIStorage()
        await asyncio.gather(*[shop_api_storage.store(data=product) for product in data])


# Only when this is the original executable file is the condition true
if __name__ == '__main__':
    asyncio.run(Crawler().run())
