import aiohttp

from crawlers.abstract_crawler.storage import StorageAbstract
from utils.check import check_status
from typing import NoReturn


class ShopAPIStorage(StorageAbstract):

    async def create_api(self, url: str, data: dict) -> bool:
        async with aiohttp.ClientSession() as session:
            async with session.post(url=url, json=data) as response:
                return await check_status(status=response.status, reason=response.reason, url=url)

    async def partial_update_api(self, url: str, data: dict) -> bool:
        async with aiohttp.ClientSession() as session:
            async with session.patch(url=url, json=data) as response:
                return await check_status(status=response.status, reason=response.reason, url=url)

    async def get_product_id(self, url: str, data: dict) -> (int, None):
        async with aiohttp.ClientSession() as session:
            async with session.post(url=url, json=data) as response:
                if await check_status(status=response.status, reason=response.reason, url=url):
                    result = await response.json()
                    return result['product_id']
        return None

    async def store(self, data: dict) -> NoReturn:
        base_url = 'http://127.0.0.1:8000'

        is_create = await self.create_api(url=f"{base_url}/api/v1/products/create", data=data)

        if not is_create:
            product_id = await self.get_product_id(url=f"{base_url}/api/v1/products/", data={"title": data['title']})

            if product_id is not None:
                new_data = {
                    "category": data['category'],
                    "store": data['store'],
                    "store_product": data['store_product'],
                }
                await self.partial_update_api(url=f"{base_url}/api/v1/products/update/{product_id}/",
                                              data=new_data)
