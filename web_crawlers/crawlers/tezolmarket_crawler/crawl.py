import asyncio
import aiohttp

from crawlers.abstract_crawler.crawl import CrawlerAbstract
from .storage import ShopAPIStorage
from utils.check import check_status
from typing import NoReturn, List, Dict


class Crawler(CrawlerAbstract):
    def __init__(self):
        self.base_url = 'https://www.tezolmarket.com'
        self.url = '/Home/GetProductQueryResult'

    async def get_params_for_download(self, category_id: int) -> List[Dict[str, int]]:
        params = {
            'CategoryId': category_id,
            'PageIndex': 1,
        }
        page_information = await self.downloader(params=params)
        max_page = page_information['NumPages']
        params_list = []
        for i in range(1, max_page):
            params_list.append({
                'CategoryId': category_id,
                'PageIndex': i,
            })

        return params_list

    async def downloader(self, params: dict) -> Dict:
        async with aiohttp.ClientSession(base_url=self.base_url) as session:
            async with session.post(url=self.url, params=params) as response:
                if await check_status(response.status, response.reason, url=f"{self.base_url}{self.url}"):
                    result = await response.json()  # JSON Response Content

        return result

    async def parser(self, product: dict, category_name: str) -> Dict:
        active = product['IsAvailable']
        if active:
            price = int(product['FinalUnitPrice'])
        else:
            price = 0

        try:
            title = f"{product['Name']} - {product['Subtitle'].replace('/', '.')}"
        except:
            title = f"{product['Name']}"

        data = {
            "title": title,
            "active": active,
            "image_url": product['LargImageUrl'],
            "category":
                {
                    "title": category_name,
                },
            "store":
                {
                    "name": "تزول مارکت",
                    "url": self.base_url,
                    "image_url": "https://www.tezolmarket.com/images/NewDesign/NewIcons/svgLogo/logo-red.svg",
                },

            "store_product": {
                "url": f"{self.base_url}/Product/{product['ProductId']}/{product['Slug']}",
                "price": price,
            }
        }

        return data

    async def run(self) -> NoReturn:
        categories = [26, 55]

        # Get params for download
        params_list_list = await asyncio.gather(
            *[self.get_params_for_download(category_id=category_id) for category_id in categories])

        # Download information
        results = []
        for params_list in params_list_list:
            results += await asyncio.gather(*[self.downloader(params=params) for params in params_list])

        # Pars information
        data = []
        for result in results:
            data += await asyncio.gather(
                *[self.parser(product=product, category_name=result['CategoryName']) for product in result['Products']])

        # Send data for shop api django project
        shop_api_storage = ShopAPIStorage()
        await asyncio.gather(*[shop_api_storage.store(data=product) for product in data])


# Only when this is the original executable file is the condition true
if __name__ == '__main__':
    asyncio.run(Crawler().run())
