from abc import abstractmethod, ABC


class CrawlerAbstract(ABC):

    @abstractmethod
    async def downloader(self, *args, **kwargs):
        pass

    @abstractmethod
    async def parser(self, *args, **kwargs):
        pass

    @abstractmethod
    async def run(self):
        pass
