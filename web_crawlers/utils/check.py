async def check_status(status: int, reason: str, url='', ) -> bool:
    if status == 200 or status == 201:
        result = True
    elif status == 400:
        result = False
    else:
        raise ValueError(f"{url} has error {status} : {reason}")

    return result
