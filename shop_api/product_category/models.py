from django.db import models


class ProductCategory(models.Model):
    title = models.CharField(max_length=150, unique=True, verbose_name='عنوان')

    class Meta:
        verbose_name = 'دسته بندی محصول'
        verbose_name_plural = 'دسته بندی های محصول'

    def __str__(self):
        return self.title
