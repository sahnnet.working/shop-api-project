from django.contrib import admin
from .models import ProductCategory


class ProductsCategoryAdmin(admin.ModelAdmin):
    list_display = ['__str__', ]

    class Meta:
        model = ProductCategory


admin.site.register(ProductCategory, ProductsCategoryAdmin)
