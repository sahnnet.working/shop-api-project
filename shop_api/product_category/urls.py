from django.urls import path
from .views import (ProductsListViewByCategory,
                    products_categories_partial_view,
                    )

app_name = 'product_category'

urlpatterns = [
    path('products/<category>/', ProductsListViewByCategory.as_view(), name='products_list_view_by_category'),
    path('products/products-categories-partial', products_categories_partial_view,
         name='products_categories_partial_view'),
]
