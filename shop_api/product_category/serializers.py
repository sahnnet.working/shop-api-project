from rest_framework import serializers
from .models import ProductCategory


class ProductCategorySerializerV1(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields = '__all__'
