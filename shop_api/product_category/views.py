from django.views.generic import ListView
from django.shortcuts import get_object_or_404, render
from .models import ProductCategory
from product.models import Product


class ProductsListViewByCategory(ListView):
    template_name = 'product/products-list.html'
    paginate_by = 30

    def get_queryset(self):
        category = self.kwargs['category']
        category = get_object_or_404(ProductCategory, title=category)

        return Product.objects.filter(categories=category, active=True)


def products_categories_partial_view(request):
    categories = ProductCategory.objects.all()

    context = {
        'categories': categories
    }

    return render(request, 'product_category/products-categories-partial.html',
                  context)
