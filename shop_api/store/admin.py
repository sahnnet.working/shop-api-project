from django.contrib import admin
from .models import Store


class StoreAdmin(admin.ModelAdmin):
    list_display = ['__str__']

    class Meta:
        model = Store


admin.site.register(Store, StoreAdmin)
