from django.db import models
from utils.os_utils import get_file_name_ext


def upload_image_path(instance, file_path):
    name, ext = get_file_name_ext(file_path)
    final_name = f"{instance.id}-{instance.name.replace('/','')}{ext}"
    return f"store/{final_name}"


class Store(models.Model):
    name = models.CharField(max_length=150, unique=True, verbose_name='نام')
    url = models.URLField(unique=True, verbose_name='آدرس وب سایت')
    image = models.ImageField(upload_to=upload_image_path, null=True, blank=True, verbose_name='تصویر')

    class Meta:
        verbose_name = 'فروشگاه'
        verbose_name_plural = 'فروشگاه ها'

    def __str__(self):
        return self.name
