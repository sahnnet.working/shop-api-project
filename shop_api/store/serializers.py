from rest_framework import serializers
from .models import Store


class StoreSerializerV1(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = '__all__'
