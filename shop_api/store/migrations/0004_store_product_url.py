# Generated by Django 4.0.1 on 2022-08-09 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0003_alter_store_name_alter_store_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='store',
            name='product_url',
            field=models.URLField(default='', unique=True, verbose_name='آدرس محصول در فروشگاه'),
        ),
    ]
