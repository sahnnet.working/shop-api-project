from django.db import models
from utils.os_utils import get_file_name_ext
from product_category.models import ProductCategory
from store.models import Store


def upload_image_path(instance, file_path):
    name, ext = get_file_name_ext(file_path)
    final_name = f"{instance.id}-{instance.title.replace('/','')}{ext}"
    return f"product/{final_name}"


class Product(models.Model):
    title = models.CharField(max_length=120, unique=True, verbose_name='عنوان')
    active = models.BooleanField(default=True, verbose_name='فعال')
    image = models.ImageField(upload_to=upload_image_path, null=True, blank=True, verbose_name='تصویر')
    categories = models.ManyToManyField(to=ProductCategory, verbose_name='دسته بندی ها')
    stores = models.ManyToManyField(to=Store, verbose_name='فروشگاه ها')

    class Meta:
        verbose_name = 'محصول'
        verbose_name_plural = 'محصولات'

    def __str__(self):
        return self.title

    def get_price(self):
        stores_product = StoreProduct.objects.filter(product=self)
        min_price = None
        for sp in stores_product:
            if min_price is None or sp.price < min_price:
                min_price = sp.price

        return min_price


class StoreProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='محصول')
    store = models.ForeignKey(Store, on_delete=models.CASCADE, verbose_name='فروشگاه')
    url = models.URLField(unique=True, verbose_name='آدرس محصول در فروشگاه اینترنتی')
    price = models.DecimalField(max_digits=30, decimal_places=0, default=0.0, verbose_name='قیمت')
