import json

from rest_framework import status
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny
from django.views.generic import ListView
from .models import Product, Store
from .serializers import ProductSerializerV1
from django.shortcuts import redirect, get_object_or_404, render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt


class ProductCreateAPIView(CreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializerV1
    permission_classes = (AllowAny,)


class ProductUpdateAPIView(UpdateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializerV1
    permission_classes = (AllowAny,)

    def perform_update(self, serializer):
        instance = serializer.instance
        data = self.request.data

        if 'category' in data.keys():
            category = serializer.get_or_create_category(category=data.pop('category'))
            if category is not None:
                instance.categories.add(category.id)

        if 'store' in data.keys():
            store = serializer.get_or_create_store(store=data.pop('store'))
            if store is not None:
                instance.stores.add(store.id)

        if 'store_product' in data.keys():
            store_product = data.pop('store_product')
            store_product['product'] = instance.id
            sp = serializer.get_or_create_store_product(store_product=store_product)
            if 'url' in store_product.keys():
                sp.url = store_product['url']

            if 'price' in store_product.keys():
                sp.price = store_product['price']
            sp.save()

        return instance


class ProductsListView(ListView):
    template_name = 'product/products-list.html'
    paginate_by = 30

    def get_queryset(self):
        return Product.objects.filter(active=True)


@csrf_exempt
def product_get_api_view(request, *args, **kwargs):
    result = HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'GET':
        result = redirect('product:products_list_view')

    elif request.method == 'POST':
        body = request.body.decode(encoding='utf-8')
        body = json.loads(body)
        data = {}

        if 'title' in body.keys():
            product = Product.objects.filter(title=body['title']).first()
            if product is not None:
                data["product_id"] = product.id

        result = JsonResponse(data)

    return result


def product_detail_view(request, *args, **kwargs):
    product = get_object_or_404(Product, id=kwargs['product_id'])
    all_store_product = product.storeproduct_set.all()

    stores_product = []
    for sp in all_store_product:
        store = Store.objects.filter(id=sp.store.id).first()
        stores_product.append({'store': store, 'url': sp.url, 'price': sp.price, })

    context = {
        'product': product,
        'stores_product': stores_product,
    }
    return render(request, 'product/product-details.html', context)
