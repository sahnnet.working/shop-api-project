from django.test import TestCase
from .models import Product


class ProductTestCase(TestCase):
    def test_create(self):
        data = {
            'title': 'شکلات صبحانه فندقی نوتلا - 750 گرم',
            'active': True,
            'price': 275000.00,
            'image': '',
        }

        product = Product.objects.create(**data)

        self.assertEqual(product.id, 1)
