from django.urls import path
from .views import (ProductCreateAPIView,
                    ProductsListView,
                    ProductUpdateAPIView,
                    product_get_api_view,
                    product_detail_view
                    )

app_name = 'product'
urlpatterns = [
    path('products/', ProductsListView.as_view(), name='products_list_view'),
    path('products/<int:product_id>/<title>', product_detail_view, name='product_detail_view'),
    path('api/v1/products/', product_get_api_view, name='product_get_api_view'),
    path('api/v1/products/create', ProductCreateAPIView.as_view(), name='product_create_api_view'),
    path('api/v1/products/update/<int:pk>/', ProductUpdateAPIView.as_view(), name='product_update_api_view'),
]
