from rest_framework import serializers
from .models import Product, StoreProduct
from store.models import Store
from product_category.models import ProductCategory
from product_category.serializers import ProductCategorySerializerV1
from store.serializers import StoreSerializerV1
from utils.request_utils import download_image


class ProductSerializerV1(serializers.ModelSerializer):
    image_url = serializers.CharField(write_only=True)
    category = serializers.DictField(write_only=True)
    store = serializers.DictField(write_only=True)
    store_product = serializers.DictField(write_only=True)

    class Meta:
        model = Product
        fields = '__all__'
        extra_kwargs = {
            'categories': {
                'required': False,
            },
            'stores': {
                'required': False,
            },
            'image': {
                'required': False,
            },
        }

    def get_or_create_store(self, store: dict):
        search = Store.objects.filter(name=store['name']).first()
        if search is None:
            image = download_image(store.pop('image_url'))
            store = StoreSerializerV1(data=store)
            if store.is_valid():
                store = store.save()
                store.image = image
                store.save()
            else:
                store = None
        else:
            store = search

        return store

    def get_or_create_category(self, category: dict):
        search = ProductCategory.objects.filter(title=category['title']).first()
        if search is None:
            category = ProductCategorySerializerV1(data=category)
            if category.is_valid():
                category = category.save()
            else:
                category = None
        else:
            category = search

        return category

    def get_or_create_store_product(self, store_product: dict):
        search = StoreProduct.objects.filter(product=store_product['product'], store=store_product['store']).first()
        if search is None:
            store_product = StoreProductV1(data=store_product)
            if store_product.is_valid():
                store_product.save()
            else:
                store_product = None
        else:
            store_product = search

        return store_product

    def create(self, validated_data):

        category = self.get_or_create_category(category=validated_data.pop('category'))

        store = self.get_or_create_store(store=validated_data.pop('store'))

        store_product = validated_data.pop('store_product')

        image = download_image(validated_data.pop('image_url'))
        product = Product.objects.create(**validated_data)
        product.image = image

        if category is not None:
            product.categories.set([category.id])

        if store is not None:
            product.stores.set([store.id])

        data = {
            "product": product.id,
            "store": store.id,
            "url": store_product['url'],
            "price": store_product['price'],
        }
        self.get_or_create_store_product(store_product=data)

        product.save()

        return product


class StoreProductV1(serializers.ModelSerializer):
    class Meta:
        model = StoreProduct
        fields = '__all__'
