from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from . import settings
from .views import home_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_view, name='home_view'),
    path('', include('product.urls', )),
    path('', include('product_category.urls', )),
]

if settings.DEBUG:
    # add root static files
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    # add root static files
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
