from django.shortcuts import redirect


def home_view(request):
    return redirect('product:products_list_view')
