import random
import requests
import io

from django.core.files import File


def download_image(url: str):
    image_binary = io.BytesIO(requests.get(url=url).content)
    image = File(file=image_binary, name=f"image{random.random()}.{url.split('.')[-1]}")

    return image
