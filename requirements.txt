Django>=4.0.1
djangorestframework>=3.13.1
aiohttp>=3.8.1
requests>=2.28.1